from django.http import JsonResponse , HttpResponse
from home.models import Author
def test(request):
	# if request.is_ajax():
	all_authors = Author.objects.all()
	new_list  = []
	for author in all_authors:
		json_author = author.__dict__
		new_author_obj  = {
			'id': json_author.get('id'),
			'name': json_author.get('name'),
			'surname': json_author.get('surname') or ''
		}
		new_list.append(new_author_obj)

	print(new_list)
	print('request from frontend!' , request.is_ajax())
	# print(request.GET.get('a'))
	# print(request.GET.get('b'))
	return JsonResponse({
		'status': True,
		'authors': new_list
	})
