1) git clone https://gitlab.com/antonmazun/wa_django.git


2) python -m venv env (windows)
3) virtualenv -p python3 env (ubuntu)
    python3 -m venv env (macOS)
    (linux/macOS) source env/bin/activate
    (windows) cd env/Scripts/ -> activate
4) cd wa_django
5) pip install -r req.txt
6) python manage.py migrate
7) python manage.py runserver 8080