from django.shortcuts import render
from django.http import HttpResponse
from .models import Book, Author


# Create your views here.


def home(request):
	all_books = Book.objects.all()  # select * from home_book;
	print('session' , dict(request.session))
	return render(request, 'home/main_page.html', {'books': all_books, 'tab': 'home'})


def about(request):
	ctx = {}
	ctx['tab'] = 'about'
	return render(request, 'home/about.html', ctx)


def book_detail(request, book_id):
	ctx = {}
	current_book = Book.objects.get(id=book_id)
	ctx['book'] = current_book
	return render(request, 'home/book_detail.html', ctx)


def author_detail(request, author_id):
	current_author = Author.objects.get(pk=author_id)
	all_books  = current_author.get_books()
	filter_books = [
		book for book in all_books if book.title.startswith('title')
	]
	print('filter_books ' , filter_books)

	# all_books_by_author = current_author.get_books()
	ctx = {}
	ctx['author'] = current_author
	# ctx['books'] = all_books_by_author
	return render(request, 'home/author_detail.html', ctx)


def calculator(request):
	print('calculator function !!!!!!')
	return render(request, 'calculator.html', {})


def add(x, y):
	return x + y


def result(request):
	print('result function !!!!!!')
	calc_obj = {'+': lambda x, y: x + y, '-': lambda x, y: x - y, '*': lambda x, y: x * y, '/': lambda x, y: x / y, }
	try:
		first_number = float(request.GET.get('first_number'))
		operation = request.GET.get('operation')
		second_number = float(request.GET.get('second_number'))
		result = calc_obj[operation](first_number, second_number)
		result_str = f'{first_number} {operation} {second_number} = {result}'
		print(first_number, operation, second_number)
		print("result ", result)
	except Exception as e:
		pass

	return render(request, 'calculator.html', {'result': result_str})


import requests as R


def exchange(request):
	current_state = R.get('https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11').json()
	user_ccy = request.GET.get('currency')
	user_count = float(request.GET.get('count'))
	result = None
	for elem in current_state:
		if elem['ccy'] == user_ccy:
			result = float(elem['sale']) * user_count
			break
	return render(request, 'calculator.html', {'result': f'Вот столько {result} грн  тебе нужно потратить!' or 'invalid operation'})
