from django.db import models


# Create your models here.

# 1) admin
# 2) ORM  - object relation mapping SQLAlchemy
# 3) ForeignKey

class Author(models.Model):
	name = models.CharField(max_length=30, verbose_name='Имя автора')
	surname = models.CharField(max_length=30, verbose_name='Фамилия автора')
	date_birth = models.DateField(verbose_name='Дата рождения')
	date_dead = models.DateField(verbose_name='Дата смерти', blank=True, null=True)  # NULL
	bio = models.TextField(max_length=5000, verbose_name='Биография автора')

	def __str__(self):
		return f'{self.name} {self.surname}'

	def front_name(self):
		return f'{self.name.capitalize()} {self.surname.capitalize()}'

	def get_books(self):
		return Book.objects.filter(author_id=self.id)

	def get_total_price(self):
		books = self.get_books()
		price_list = []
		for book in books:
			if book.is_sale:
				price_list.append(book.new_price or 0)
			else:
				price_list.append(book.price or 0)
		return sum(price_list)

	def count_is_sale(self):
		return len(self.get_books().filter(is_sale=True))

	def count_is_not_sale(self):
		return len(self.get_books().filter(is_sale=False))


class Book(models.Model):
	GENRE_CHOICE = (('comedy', 'Комедия'), ('trag', 'Трагедия'), ('melodrama', 'Мелодрама'),)
	author = models.ForeignKey(Author, on_delete=models.CASCADE, default='', null=True, blank=True)
	title = models.CharField(max_length=255)
	desctiption = models.TextField(max_length=1500)
	price = models.FloatField()
	is_sale = models.BooleanField(default=False)
	new_price = models.FloatField(blank=True, null=True)
	genre = models.CharField(choices=GENRE_CHOICE, max_length=50)

	def __str__(self):
		return f'title - {self.title}'
